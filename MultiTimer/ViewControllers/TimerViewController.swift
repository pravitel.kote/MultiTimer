//
//  TimerViewController.swift
//  MultiTimer
//
//  Created by Александра on 07.07.2021.
//

import UIKit

   // MARK: - TimerViewController
    
final class TimerViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: - Private property
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(TimerTableViewCell.self, forCellReuseIdentifier: "TimerCell")
        return tableView
    }()
    
    private let nameTextField = UITextField()
    private let timeTextField = UITextField()
    private let timerButton = UIButton()
    private let constantValue: CGFloat = 31
    
    private var timer: Timer?
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        self.navigationItem.title = "Мультитаймер"
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupTableView()
    }
    
    
    // MARK: - Private methods
    
    private func setupView() {
        setupTimerNameTextfield()
        setupTimerTimeTextfield()
        setupTimerButton()
        setupTableView()
    }
    
    private func setupTimerNameTextfield() {
        nameTextField.borderStyle = .roundedRect
        nameTextField.contentVerticalAlignment = .center
        nameTextField.textAlignment = .left
        nameTextField.placeholder = "Название таймера"
        nameTextField.delegate = self
        nameTextField.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(nameTextField)
        
        nameTextField.anchor(top: view.topAnchor,
                             paddingTop: 80,
                             leading: view.leadingAnchor,
                             paddingLeading: 10,
                             trailing: view.trailingAnchor,
                             paddingTrailing: 10,
                             height: 31)
    }
    
    private func setupTimerTimeTextfield() {
        timeTextField.borderStyle = .roundedRect
        timeTextField.contentVerticalAlignment = .center
        timeTextField.textAlignment = .left
        timeTextField.placeholder = "Время в секундах"
        timeTextField.delegate = self
        timeTextField.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(timeTextField)
        
        
        timeTextField.anchor(top: nameTextField.bottomAnchor,
                             paddingTop: 10,
                             leading: view.leadingAnchor,
                             paddingLeading: 10,
                             trailing: view.trailingAnchor,
                             paddingTrailing: 10,
                             height: 31)
    }
    
    private func setupTimerButton() {
        timerButton.setTitle("Добавить", for: .normal)
        timerButton.backgroundColor = .orange
        timerButton.setTitleColor(UIColor.white, for: .normal)
        timerButton.layer.cornerRadius = 15
        timerButton.addTarget(self, action: #selector(self.buttonTapped), for: .touchUpInside)
        timerButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(timerButton)
        
        timerButton.anchor(top: timeTextField.bottomAnchor,
                           paddingTop: 10,
                           leading: view.leadingAnchor,
                           paddingLeading: 10,
                           trailing: view.trailingAnchor,
                           paddingTrailing: 10,
                           height: 45)
    }
    
    private func setupTableView() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([tableView.topAnchor.constraint(equalTo: timerButton.bottomAnchor, constant: 10),
                                     tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                                     tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                                     tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
                                     
        ])
        
        tableView.dataSource = self
    }
    
    private func updateTimerInfo() {
        DispatchQueue.main.async {
            guard let seconds = Int(self.timeTextField.text ?? "0") else { return }
            
            let newTimer = TimerInfo(name: self.nameTextField.text ?? "Новый таймер", timeInSeconds: seconds)
            
            TimerStore.shared.addTimerInfo(newTimer)
            
            let indexPath = IndexPath(row: TimerStore.shared.timers.count - 1, section: 0)
            
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: [indexPath], with: .top)
            self.tableView.endUpdates()
        }
    }

    
    
    // MARK: - Actions
    
    @objc
    private func buttonTapped(sender : UIButton) {
        if timer == nil {
            let timer = Timer.scheduledTimer(timeInterval: 1.0,
                                             target: self,
                                             selector: #selector(updateTimer),
                                             userInfo: nil,
                                             repeats: true)
            RunLoop.current.add(timer, forMode: .common)
            timer.tolerance = 0.1
            
            self.timer = timer
        }
        updateTimerInfo()
    }
    
    @objc
    private func updateTimer() {
        guard let visibleRowsIndexPaths = tableView.indexPathsForVisibleRows else { return }
        
        for indexPath in visibleRowsIndexPaths {
            if let cell = tableView.cellForRow(at: indexPath) as? TimerTableViewCell {
                cell.updateTime()
            }
        }
    }
}


// MARK: - UITableViewDataSource

extension TimerViewController: UITableViewDataSource, TimerOff {
    
    func deleteTimer(indexPath: IndexPath) {
        timer = nil
        self.tableView.beginUpdates()
        self.tableView.deleteRows(at: [indexPath], with: .automatic)
        self.tableView.endUpdates()
    }
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return TimerStore.shared.timers.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TimerCell",
                                                       for: indexPath) as? TimerTableViewCell else { return UITableViewCell() }
        
        let timer = TimerStore.shared.timers[indexPath.row]
        cell.delegate = self
        cell.startTime = Date()
        cell.configureCell(for: timer)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Таймеры"
    }
    
}

