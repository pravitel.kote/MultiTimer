//
//  SceneDelegate.swift
//  MultiTimer
//
//  Created by Александра on 07.07.2021.
//

import UIKit

final class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        let viewController = TimerViewController()
        let navViewController = UINavigationController(rootViewController: viewController)
        

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navViewController
        window?.backgroundColor = .white
        window?.makeKeyAndVisible()

        guard let windowScene = (scene as? UIWindowScene) else { return }
        window?.windowScene = windowScene
    }

}
