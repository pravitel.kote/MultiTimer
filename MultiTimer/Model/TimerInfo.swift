//
//  TimerInfo.swift
//  MultiTimer
//
//  Created by Александра on 07.07.2021.
//

import Foundation

// MARK: - TimerInfo

struct TimerInfo: Equatable {
  let name: String
  let timeInSeconds: Int
}
